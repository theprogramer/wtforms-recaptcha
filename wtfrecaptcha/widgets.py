from wtforms.widgets import HTMLString

# Template for the widget
RECAPTCHA_HTML = HTMLString(u"""<script src="//www.google.com/recaptcha/api.js"></script><div class="g-recaptcha" data-sitekey="{{public_key}}"></div>""")

class Recaptcha(object):
    """Recaptcha widget that displays HTML depending on security status"""

    def __call__(self, field, **kwargs):
        html = RECAPTCHA_HTML.replace('{{public_key}}', field.public_key)
        return html
